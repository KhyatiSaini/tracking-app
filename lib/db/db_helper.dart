import 'package:latlng/latlng.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:tracking_app/models/coordinates_data.dart';
import 'package:tracking_app/models/location_time.dart';
import 'package:tracking_app/models/track_data.dart';
import 'package:tracking_app/utilities/date_format.dart';

import '../utilities/constant.dart';

class DbHelper {
  static late Database db;

  static Future openDataBase() async {
    final database = openDatabase(
      join(await getDatabasesPath(), dbName),
      onCreate: (db, version) {
        // Run the CREATE TABLE statement on the database.
        return db.execute(
          'CREATE TABLE $tableName($dateColumn TEXT, $timeColumn TEXT, $latColumn REAL, $lonColumn REAL)',
        );
      },
      version: 1,
    );

    db = await database;
  }

  static Future insertLocationData(CoordinatesData coordinatesData) async {
    await db.insert(
      tableName,
      coordinatesData.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  static Future<List<TrackData>> readLocationData() async {
    final List<Map<String, dynamic>> maps = await db.query(tableName);
    List<CoordinatesData> list = List.generate(maps.length, (index) {
      return CoordinatesData.fromJson(maps[index]);
    });

    List<TrackData> trackData = [];
    if (list.isNotEmpty) {
      String date = list.first.date;
      List<LocationTime> coordinatesList = [];
      for (int i = 0; i < list.length; i++) {
        if (date == list[i].date) {
          coordinatesList.add(
            LocationTime(
              time: list[i].time,
              latLng: LatLng(
                list[i].latitude.toDouble(),
                list[i].longitude.toDouble(),
              ),
            ),
          );
        } else {
          trackData.add(
            TrackData(
              date: date,
              coordinatesWithTime: coordinatesList,
            ),
          );
          date = list[i].date;
          coordinatesList = [];
          coordinatesList.add(
            LocationTime(
              time: list[i].time,
              latLng: LatLng(
                list[i].latitude.toDouble(),
                list[i].longitude.toDouble(),
              ),
            ),
          );
        }
      }
      dateFormat(date);
      trackData.add(
        TrackData(
          date: date,
          coordinatesWithTime: coordinatesList,
        ),
      );
    }

    return trackData;
  }
}
