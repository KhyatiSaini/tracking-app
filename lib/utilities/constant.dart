const String dbName = 'location.db';
const String tableName = 'coordinates';
const String dateColumn = 'date';
const String timeColumn = 'time';
const String latColumn = 'lat';
const String lonColumn = 'lon';
