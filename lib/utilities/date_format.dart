String dateFormat(String dateTime) {
  String date = '${dateTime.substring(8, 10)} ${month(int.parse(dateTime.substring(5, 7)))} ${dateTime.substring(0, 4)}';
  return date;
}

String month(int month) {
  switch (month) {
    case 1:
      return 'Jan';
    case 2:
      return 'Feb';
    case 3:
      return 'Mar';
    case 4:
      return 'Apr';
    case 5:
      return 'May';
    case 6:
      return 'Jun';
    case 7:
      return 'Jul';
    case 8:
      return 'Aug';
    case 9:
      return 'Sep';
    case 10:
      return 'Oct';
    case 11:
      return 'Nov';
    case 12:
      return 'Dec';
  }
  return '';
}
