import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:location/location.dart';
import 'package:tracking_app/db/db_helper.dart';
import 'package:tracking_app/models/coordinates_data.dart';
import 'package:tracking_app/services/location_services.dart';

class LocationProvider extends ChangeNotifier {
  static const Duration defaultInterval = Duration(
    seconds: 5,
  );

  late bool isTrackingStopped;
  late LocationData? locationData;

  factory LocationProvider() => _locationProvider;

  LocationProvider._() {
    isTrackingStopped = false;
  }

  static final LocationProvider _locationProvider = LocationProvider._();

  Duration checkInterval = defaultInterval;
  Timer? _timer;

  Future trackLocation([Timer? timer]) async {
    _timer?.cancel();
    timer?.cancel();

    if (isTrackingStopped) {
      return;
    }

    locationData = await LocationServices().determineLocation();

    if (locationData != null) {
      DateTime dateTime = DateTime.now();
      int year = dateTime.year;
      int month = dateTime.month;
      int day = dateTime.day;

      String date = '$year-';
      date += (month < 10) ? '0$month-' : '$month-';
      date += (day < 10) ? '0$day' : '$day';

      int hour = dateTime.hour;
      int minute = dateTime.minute;
      int second = dateTime.second;
      String time = (hour < 10) ? '0$hour:' : '$hour:';
      time += (minute < 10) ? '0$minute:' : '$minute:';
      time += (second < 10) ? '0$second' : '$second';

      CoordinatesData coordinatesData = CoordinatesData(
        latitude: locationData!.latitude!,
        longitude: locationData!.longitude!,
        date: date,
        time: time,
      );
      DbHelper.insertLocationData(coordinatesData);
    }

    _timer = Timer(checkInterval, trackLocation);
  }

  void startTracking() {
    isTrackingStopped = false;
    trackLocation();
  }

  void stopTracking() {
    isTrackingStopped = true;
    notifyListeners();
  }

  void resetData() {
    locationData = null;
  }
}
