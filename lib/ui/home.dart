import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tracking_app/db/db_helper.dart';
import 'package:tracking_app/models/track_data.dart';
import 'package:tracking_app/providers/location_provider.dart';
import 'package:tracking_app/utilities/date_format.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool trackStart = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Track Location',
          style: TextStyle(
            color: Colors.grey.shade300,
          ),
        ),
      ),
      body: FutureBuilder<List<TrackData>>(
        future: DbHelper.readLocationData(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.none ||
              snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          } else {
            List<TrackData> trackDataList = snapshot.data ?? [];
            if (trackDataList.isEmpty) {
              return Center(
                child: Container(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    'Tap on the floating button for tracking your location',
                    style: TextStyle(
                      color: Colors.grey.shade500,
                      fontSize: 16,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              );
            } else {
              return Container(
                margin: const EdgeInsets.all(16),
                padding: const EdgeInsets.symmetric(
                  vertical: 8,
                ),
                decoration: BoxDecoration(
                    color: Colors.grey.shade800,
                    borderRadius: BorderRadius.circular(10)),
                child: ListView.builder(
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    TrackData trackData = trackDataList[index];
                    return Container(
                      padding: const EdgeInsets.symmetric(
                        vertical: 8,
                        horizontal: 16,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    dateFormat(trackData.date),
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.cyan.shade600,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                  Container(
                                    height: 2,
                                    width: 80,
                                    margin: const EdgeInsets.only(
                                      top: 2,
                                    ),
                                    decoration: BoxDecoration(
                                      color: Colors.cyan.shade600,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Container(
                                  padding: const EdgeInsets.all(8),
                                  margin: const EdgeInsets.only(
                                    top: 10,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Colors.cyan.shade400,
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Text(
                                    'Time'.toUpperCase(),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.cyan.shade900,
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              Expanded(
                                child: Container(
                                  padding: const EdgeInsets.all(8),
                                  margin: const EdgeInsets.only(
                                    top: 10,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Colors.cyan.shade400,
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Text(
                                    'Latitude'.toUpperCase(),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.cyan.shade900,
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              Expanded(
                                child: Container(
                                  padding: const EdgeInsets.all(8),
                                  margin: const EdgeInsets.only(
                                    top: 10,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Colors.cyan.shade400,
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Text(
                                    'Longitude'.toUpperCase(),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.cyan.shade900,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (context, i) {
                              return Row(
                                children: [
                                  Expanded(
                                    child: Container(
                                      padding: const EdgeInsets.all(8),
                                      margin: const EdgeInsets.only(
                                        top: 10,
                                      ),
                                      decoration: BoxDecoration(
                                        color: Colors.grey.shade600,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                      child: Text(
                                        trackData.coordinatesWithTime[i].time,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.grey.shade400,
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Expanded(
                                    child: Container(
                                      padding: const EdgeInsets.all(8),
                                      margin: const EdgeInsets.only(
                                        top: 10,
                                      ),
                                      decoration: BoxDecoration(
                                        color: Colors.grey.shade600,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                      child: Text(
                                        trackData.coordinatesWithTime[i].latLng
                                            .latitude
                                            .toString(),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.grey.shade400,
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Expanded(
                                    child: Container(
                                      padding: const EdgeInsets.all(8),
                                      margin: const EdgeInsets.only(
                                        top: 10,
                                      ),
                                      decoration: BoxDecoration(
                                        color: Colors.grey.shade600,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                      child: Text(
                                        trackData.coordinatesWithTime[i].latLng
                                            .longitude
                                            .toString(),
                                        style: TextStyle(
                                          color: Colors.grey.shade400,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ],
                              );
                            },
                            itemCount: trackData.coordinatesWithTime.length,
                          ),
                        ],
                      ),
                    );
                  },
                  itemCount: trackDataList.length,
                ),
              );
            }
          }
        },
      ),
      floatingActionButton: Consumer<LocationProvider>(
          builder: (context, locationProvider, child) {
        return FloatingActionButton(
          onPressed: () async {
            if (!trackStart) {
              setState(() {
                trackStart = true;
              });
              locationProvider.startTracking();
            } else {
              setState(() {
                trackStart = false;
              });
              locationProvider.stopTracking();
              locationProvider.resetData();
            }
          },
          child: Icon(
            !trackStart ? Icons.share_location : Icons.stop_sharp,
            color: Colors.grey.shade800,
          ),
          backgroundColor: !trackStart ? Colors.cyan : Colors.redAccent,
        );
      }),
    );
  }
}
