import 'package:latlng/latlng.dart';

class LocationTime {
  final String time;
  final LatLng latLng;

  LocationTime({
    required this.time,
    required this.latLng,
  });

  @override
  String toString() {
    return 'LocationTime{time: $time, latLng: $latLng}';
  }
}
