class CoordinatesData {
  final String date;
  final String time;
  final num latitude;
  final num longitude;

  CoordinatesData({required this.date, required this.time, this.latitude = 0, this.longitude = 0});

  factory CoordinatesData.fromJson(Map<String, dynamic> json) {
    return CoordinatesData(
      date: json['date'] as String,
      time: json['time'] as String,
      latitude: json['lat'] as num,
      longitude: json['lon'] as num,
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = <String, dynamic>{};
    json['date'] = date;
    json['time'] = time;
    json['lat'] = latitude;
    json['lon'] = longitude;
    return json;
  }

  @override
  String toString() {
    return 'CoordinatesData{date: $date, time: $time, latitude: $latitude, longitude: $longitude}';
  }
}
