import 'package:tracking_app/models/location_time.dart';

class TrackData {
  final String date;
  final List<LocationTime> coordinatesWithTime;

  TrackData({
    required this.date,
    required this.coordinatesWithTime,
  });

  @override
  String toString() {
    return 'TrackData{date: $date, coordinatesWithTime: $coordinatesWithTime}';
  }
}
