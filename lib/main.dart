import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tracking_app/db/db_helper.dart';
import 'package:tracking_app/providers/location_provider.dart';
import 'package:tracking_app/ui/home.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Provider.debugCheckInvalidValueType = null;
  await DbHelper.openDataBase();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(
          create: (context) => LocationProvider(),
        ),
      ],
      child: MaterialApp(
        title: 'Location Tracking App',
        theme: ThemeData(
          primarySwatch: Colors.cyan,
          brightness: Brightness.dark,
        ),
        home: const HomePage(),
      ),
    );
  }
}
