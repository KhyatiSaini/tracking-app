import 'package:location/location.dart';

class LocationServices {
  Location location = Location();
  LocationData? locationData;

  Future<LocationData?> determineLocation() async {
    bool locationEnabled = await checkLocationServiceStatus();
    if (!locationEnabled) {
      locationEnabled = await location.requestService();
      if (!locationEnabled) {
        return null;
      }
    }

    bool permissions = await checkPermissions();
    if (!permissions) {
      await location.requestPermission();
      permissions = await checkPermissions();
      if (!permissions) {
        return null;
      }
    }

    locationData = await location.getLocation();

    return locationData;
  }

  Future<bool> checkLocationServiceStatus() async {
    return await location.serviceEnabled();
  }

  Future<bool> checkPermissions() async {
    bool granted = false;
    PermissionStatus permissionStatus = await location.hasPermission();
    if (permissionStatus == PermissionStatus.granted || permissionStatus == PermissionStatus.grantedLimited) {
      granted = true;
    }
    return granted;
  }

}